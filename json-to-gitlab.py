import os
import json
import html2text

src_root = os.getenv("SRC_ROOT", "")
jsp_root = os.getenv("JSP_ROOT", "")

severities = {
    "0": "Info", 
    "1": "Low",
    "2": "Low",
    "3": "Medium",
    "4": "High",
    "5": "Critical"
}

gl_results = {
    "version": "2.0",
    "vulnerabilities": []
}

def make_identifier(reference, url):
    return {
        "type": reference,
        "name": reference,
        "value": reference,
        "url": url
    }

with open("results.json", "r") as r:
    results_json = json.load(r)
    results = results_json['results']

    for finding in results.get('TestResults', {}).get('Issues', {}).get('Issue', []):
        finding_hash = finding["IssueId"] + finding["FlawMatch"]["FlawHash"] + finding["FlawMatch"]["ProcedureHash"] \
                       + finding["FlawMatch"]["PrototypeHash"] + finding["FlawMatch"]["CauseHash"] \
                       + finding["FlawMatch"]["CauseHashOrdinal"] + finding["FlawMatch"]["CauseHashCount"] \
                       + finding["FlawMatch"]["FlawHashOrdinal"] + finding["FlawMatch"]["FlawHashCount"]

        finding_filename = finding["Files"]["SourceFile"]["File"]
        finding_filename_prefix = jsp_root if finding_filename.startswith("WEB-INF") else src_root
        finding_filename = finding_filename_prefix + finding_filename

        identifiers = []
        finding_description = html2text.html2text(finding["DisplayText"], bodywidth=0)
        description_split = finding_description.split(" References: ")
        if len(description_split) == 2:
            finding_description = description_split[0]
            references = description_split[1]
            references_split = references[1:-1].split(") [")
            for reference in references_split:
                reference_split = reference.split("](")
                identifiers.append(make_identifier(reference_split[0], reference_split[1]))

        gl_results["vulnerabilities"].append({
            "category": "sast",
            "name": finding["IssueType"],
            "message": finding["IssueType"],
            "description": finding_description,
            "cve": finding_hash,
            "severity": severities[finding["Severity"]],
            "confidence": "High",
            "scanner": {
                "id": "veracode_sast",
                "name": "Veracode SAST"
            },
            "location": {
                "file": finding_filename,
                "start_line": int(finding["Files"]["SourceFile"]["Line"]),
                "end_line": int(finding["Files"]["SourceFile"]["Line"]),
                "class": finding["Files"]["SourceFile"]["Scope"],
                "method": finding["Files"]["SourceFile"]["FunctionName"],
                "dependency": {
                    "package": {}
                }
            },
            "identifiers": identifiers
        })

with open("report.json", "w") as f:
    f.write(json.dumps(gl_results, indent=4))
